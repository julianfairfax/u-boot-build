[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# u-boot-build
GitLab and Codeberg CI workflows to build u-boot for a few Rockchip boards. This project is based on [Kwiboo/u-boot-build](https://github.com/Kwiboo/u-boot-build).

### Source repositories

- [Kwiboo/u-boot-rockchip](https://github.com/Kwiboo/u-boot-rockchip)
- [rockchip-linux/rkbin](https://github.com/rockchip-linux/rkbin)

### Produced artifacts

 - `u-boot-rockchip.bin` for use with SD card or eMMC module
 - `u-boot-rockchip-spi.bin` for use with SPI flash

These files contain `idbloader.img` and `u-boot.itb` at their expected offsets.

## Flashing

### SD card or eMMC module
Write the `u-boot-rockchip.bin` image to sector 64 of a SD card or eMMC module (assumed to be /dev/mmcblk0):

```
dd if=u-boot-rockchip.bin of=/dev/mmcblk0 bs=32k seek=1 conv=fsync
```

### SPI flash

Write the `u-boot-rockchip-spi.bin` image to begining of SPI flash (assumed to be /dev/mtd0) using `flashcp`:

```
flashcp -v -p u-boot-rockchip-spi.bin /dev/mtd0
```

Or using U-Boot cmdline:

- Put `u-boot-rockchip-spi.bin` on first partition of a SD card.
- Run from U-Boot cmdline:
```
sf probe

load mmc 1:1 10000000 u-boot-rockchip-spi.bin

sf update $fileaddr 0 $filesize
```

Erase U-Boot from SPI flash (assumed to be /dev/mtd0) using `flash_erase`:

```
flash_erase /dev/mtd0 0 16
```

Or erase using U-Boot cmdline:

```
sf probe

sf erase 0 +10000
```
